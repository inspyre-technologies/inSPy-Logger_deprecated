from setuptools import setup

setup(
    name='inSPy-Logger',
    version='2.0.0-alpha.2',
    packages=['inspy_logger'],
    url='https://softworks.inspyre.tech/inSPy-Logger',
    license='GNU',
    author='Taylor-Jayde Blackstone',
    author_email='t.blackstone@inspyre.tech',
    description='Colorable, scalable logger for CLI'
)
